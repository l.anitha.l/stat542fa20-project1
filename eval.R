data <- read.csv("Ames_data.csv")
testIDs <- read.table("project1_testIDs.dat")

sink("log.txt")
for (i in 1:10) {
  train <- data[-testIDs[,i], ]
  test <- data[testIDs[,i], ]
  test.y <- test[, c(1, 83)]
  test <- test[, -83]
  write.csv(train,"train.csv",row.names=FALSE)
  write.csv(test, "test.csv",row.names=FALSE)
  
  cat(paste(format(Sys.time(), usetz = FALSE), "|------- Evaluating split:", i, "-------"), sep = "\n")
  start.time <- Sys.time()
  source("mymain.R")
  end.time <- Sys.time()
  time.taken <- difftime(end.time, start.time, units="secs")
  names(test.y)[2] <- "True_Sale_Price"
  pred1 <- read.csv("mysubmission1.txt")
  pred1 <- merge(pred1, test.y, by="PID")
  rmse1 <- sqrt(mean((log(pred1$Sale_Price) - log(pred1$True_Sale_Price))^2))
  pred2 <- read.csv("mysubmission2.txt")
  pred2 <- merge(pred2, test.y, by="PID")
  names(test.y)[2] <- "True_Sale_Price"
  rmse2 <- sqrt(mean((log(pred2$Sale_Price) - log(pred2$True_Sale_Price))^2))
  # rmse <- 0.2
  cat(paste(format(Sys.time(), usetz = FALSE), "|------- RMSE:", rmse1, "-------"), sep = "\n")
  cat(paste(format(Sys.time(), usetz = FALSE), "|------- RMSE:", rmse2, "-------"), sep = "\n")
  cat(paste(format(Sys.time(), usetz = FALSE), "|------- time.taken:", time.taken, "seconds -------"), sep = "\n")
}
sink()
